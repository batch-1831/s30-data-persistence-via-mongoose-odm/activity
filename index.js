// ODM - Object Document Mapper 
// Mongoose - ODM library that manages data relationships, validates schemas and simplifies MongoDb manipulation
// Schema - representation of a document's structure. It also contains a document's expected properties and data types.
// Models - a programming interface for querying or manipulating a database. A mongoose model contains methods that simplify such operations.

// Before doing the project first do:
// npm init -y
// npm install express
// code .gitignore    // para di maisama si /node_modules folder sa pag git push
// npm install mongoose

// include express module
const express = require("express");
// Mongoose - is a package that allows creation of Schemas to model our data structure.
//          - Also has access to a number of methods for manipulating our database.
const mongoose = require("mongoose");

// setup the express server
const app = express();

// List the port where the server will listen
const port = 3001;

// MongoDB Connection
// Connect to the database by passing in your connection string
/*
    Syntax:
        mongoose.connect("<MongoDB Atlas Connection String>", 
        {useNewUrlParser: true, useUnifiedTopology: true});
*/
                                                        // mongodb.net/ <CREATE DATABASE-NAME> ?retry
mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.u7e9e.mongodb.net/b183_to-do?retryWrites=true&w=majority", {useNewUrlParser: true, useUnifiedTopology: true});

// Set a notification for connection success or error with our database.
let db = mongoose.connection;

// If a connection error occured, it will be output in the console.
// console.error.bind(console) - allows us to print the error in the browser consoles and in the terminal
db.on("error", console.error.bind(console, "connection error"));

// If the connection is successful, a console message will be shown.
db.once("open", () => {console.log("We're connected to the cloud database.")})


// Mongoose Schema
//      - Determine the structure of the document to be written in the database
//      - Schemas act as blueprints to our data
const taskSchema = new mongoose.Schema({
    // Name of the task
    name: String, //String is a shorthand for name: {type: String}
    // Status of the task (Complete, Pending, Incomplete)
    status: {
        type: String,
        // Default values are the predefined values for a field if we don't put any value.
        default: "Pending"
    }
})


// Mongoose Model
// Model uses Schemeas and they act as the middleman from the server (JS code) to our database.
/*

    Syntax: 
        const modelName = mongoose.model("collectionName", mongooseSchema);

*/
    // Model must be in singular form and capitalize the first letter.
	// The variable/object "Task" can now used to run commands for interacting with our database
	// "Task" is both capitalized following the MVC approach for naming conventions
	// Models must be in singular form and capitalized
	// The first parameter of the Mongoose model method indicates the collection in where to store the data
	// The second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection
	// Using Mongoose, the package was programmed well enough that it autotimatically converts the singular form of the model name into a plural form when creating a collection.

const Task = mongoose.model("Task", taskSchema);

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Route for creating a task

// Business Logic
/*
        1. Add a functionality to check if there are duplicate tasks
            - If the task already exists in the database, we return an error
            - If the task doesn't exist in the database, we add it in the database
        2. The task data will be coming from the request's body
        3. Create a new Task object with a "name" field/property
        4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object

        Task is duplicated if: 
        - result from the query not equal to null.
        - req.body.name is equal to result.name
*/

app.post("/tasks", (req, res) => {
    // Call back functions in mongoose methods are programmed this way:
        // first parameter store the error
        // second parameter return the result
        // req.body.name = eat
        // Task.findOne({name: eat})
    Task.findOne({name: req.body.name}, (err, result) => {
        console.log(result);

        // If a document was found and the document's name matches the information sent by the client/postman
        if(result != null && req.body.name == result.name) {
            // Return a message to the client or postman
            return res.send("Duplicate task found!")
        }
        // If no document was found or no duplicate
        else {
            // Create a new task and save to database

            let newTask = new Task({
                name: req.body.name,
            });

            // The ".save" method will store the information to the database
            // Since the "newTask" was created/instantiated from the Task model that contains the Mongoose Schema, so it will gain access to the save method.

            newTask.save((saveErr, saveTask) => {
                // If there are error in saving it will be displayed in the console.
                if(saveErr) {
                    return console.error(saveErr);
                }
                // If no error found while creating the document it will be save in the database
                else {
                    return res.status(201).send("New task created.");
                }
            })
        }
    })
})

// Business Logic
	/*
    1. Retrieve all the documents
    2. If an error is encountered, print the error
    3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/
app.get("/tasks", (req, res) => {

    // .find({}) -> find all
    Task.find({}, (err, result) => {
        if(err) {
            return console.log(err)
        }
        else {
            res.status(200).send(result)
        }
    })
})

// Activity
/*

	Instructions s30 Activity:
	1. Create a User schema.
	2. Create a User model.
	3. Create a POST route that will access the "/signup" route that will create a user.
	4. Process a POST request at the "/signup" route using postman to register a user.
	5. Create a git repository named S30.
	6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
	7. Add the link in Boodle.

*/
//Register a user

/*
	Business Logic:

	1. Add a functionality to check if there are duplicate tasks
		- If the user already exists in the database, we return an error
		- If the user doesn't exist in the database, we add it in the database
	2. The user data will be coming from the request's body
		- Note: Make sure that the username and password from the request is not empty.
	3. Create a new User object with a "username" and "password" fields/properties

    user if duplicated if:
        - result = req.body.name
        - result != null
*/

// User Schema
const userSchema = mongoose.Schema({
    name: String,
    password: String
})

// User model
const User = mongoose.model("User", userSchema);

// Register user
app.post("/signup", (req, res) => {
    
    User.findOne({name: req.body.name}, (err, result) => {

        // if user exists
        if(result != null && req.body.name == result.name) {
            return res.send("Duplicate User Found!")
        }
        // if user and password are blank
        else if(req.body.name == "" && req.body.password == "") {
            return res.send("Please input user name and password!");
        }
        // if no existing user is found
        else {
            // create new user object
            let newUser = new User({
                name: req.body.name,
                password: req.body.password
            })
            
            // save user to database
            newUser.save((saveUserErr, saveUser) => {
                if(saveUserErr) {
                    return console.log(saveUserErr);
                }
                else{
                    return res.status(201).send(`User ${req.body.name} is registered.`);
                }
            })
        }
    })
})

// Find all users
app.get("/users", (req, res) => {

    User.find({}, (err, result) => {
        if(err) {
            return console.log(err)
        }
        else {
            res.status(200).send(result)
        }
    })
})

//Listen to the port
app.listen(port, () => console.log(`Sever running at port ${port}`));